const http = require('http')
const PORT = 3001

const server = http.createServer((req,res)=>{
    console.log('Server is starting')
    res.write('Hello world')
    res.end()
})


server.listen(PORT,()=> console.log('Server started...'))
