FROM mcr.microsoft.com/playwright:v1.22.0-focal

WORKDIR /app

COPY package.json /app

RUN npm install

COPY . .

EXPOSE 3001

CMD ["node","app.js"]
